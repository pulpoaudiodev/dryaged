/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIConfiguration.hpp
//  DryAged
//
//  Created by Rudolf Leitner on 20/02/17.
//
//

#ifndef UIConfiguration_hpp
#define UIConfiguration_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>

class UIElementConfig {
public:
    std::string name;
    std::string lookAndFeel;
    int x;
    int y;
    int w;
    int h;
    int sizeX;
    int sizeY;
    std::string param;
    bool visible;
    bool showTextBox;
    
    UIElementConfig();
    
    bool hasBounds();
    bool hasSize();
};

class UIConfig {
    std::vector<UIElementConfig> elementConfigs;

public:
    int w;
    int h;
    bool showBackground;
    bool showReloadPatchesButton;
    bool showReloadUIButton;
    int aboutX;
    int aboutY;
    int aboutH;
    int aboutW;
    
    std::map<std::string, juce::LookAndFeel*> lookAndFeels;
    std::string licenceText;
    
    
    UIConfig();
    ~UIConfig();
    
    void addElementConfig( UIElementConfig& aElementConfig);
    UIElementConfig* getElementConfig( std::string aName);
    
    void cleanup();
    
    juce::LookAndFeel* getLookAndFeel( std::string& aName );
    void addLookAndFeel( std::string& aName, juce::LookAndFeel* aLookAndFeel);
    
    
};



#endif /* UIConfiguration_hpp */
