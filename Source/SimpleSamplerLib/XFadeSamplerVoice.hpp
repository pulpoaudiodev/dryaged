/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  XFadeSamplerVoice.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 03/12/16.
//
//

#ifndef XFadeSamplerVoice_hpp
#define XFadeSamplerVoice_hpp

#include <stdio.h>
#include "JuceHeader.h"
#include "PatchDefinition.hpp"
#include "PluginParameters.hpp"
#include "SampleBin.hpp"
#include "DFDThreadPool.hpp"


/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerSound
 * forward definition
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class XFadeSamplerSound;


/*--------------------------------------------------------------------------------------------------------------
 * ElementPlayData
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
typedef struct {
    float sourceSamplePosition;
    int nrOfSamples;
    float pitchRatio;
    float gain;
    float elementGain;
    float* inL;
    float* inR;
    bool playing;
    
    float velocityRangeFrom;
    float velocityRangeTo;
    float xFadeVelocityTo;


    SampleBinSample* binSample;
    DFDRingBuffer* ringBuffer;

} ElementPlayData;



/*--------------------------------------------------------------------------------------------------------------
 * EnvParameters
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class EnvParameters {
public:
	float offsetS;
	float gain0;
	std::vector<float> t;
	std::vector<float> gains;
	float releaseS;
	bool logarithmic;


	EnvParameters() : offsetS(0.0f), gain0(6.3e-8f), releaseS(0.0f), logarithmic(false)
	{}

	void setOffsetS( float aOffsetS ) { offsetS = aOffsetS;};
	float getOffsetS() { return offsetS;};
	void setGain0( float aGain0 ) { gain0 = aGain0; };
	void addEnvParameter( float timeS, float gain);
	void setTime( int index, float timeS);
    float getTime( int index);
	void setGain( int index, float gain);
	void setParameter( int index, float timeS, float decibel );
	void setReleaseS( float aReleaseS ) { releaseS = aReleaseS; };

	void setOAHDSR( float aOffsetS, float aAttackS, float aHoldS, float aDecayS, float aSustainGain, float aReleaseS );
};


/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerVoice
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class XFadeSamplerVoice    : public SynthesiserVoice
{
private:
    int soundingMidiNote;

public:
    //==============================================================================
    /** Creates a SamplerVoice. */
    XFadeSamplerVoice(PluginParameters* aPluginParameters, SampleBin* aSampleBin, ThreadPool *aDfdThreadPool);
    
    /** Destructor. */
    ~XFadeSamplerVoice();
    
    //==============================================================================
    bool canPlaySound (SynthesiserSound* aSound) override;
    
    
    void startNote (int midiNoteNumber, float aVelocity, SynthesiserSound* aSound, int pitchWheel) override;
    void stopNote (float velocity, bool allowTailOff) override;
    
    void pitchWheelMoved (int newValue) override;
    void controllerMoved (int controllerNumber, int newValue) override;
    void pressureMoved( float newValue);
    void pressureCatchUp( float newValue);
    void pressureCatchDown( float newValue);
    
    void renderNextBlock (AudioSampleBuffer&, int startSample, int numSamples) override;
    
    bool isPlayingLayer( int layerNr );
    
    
    
    void calcXFade( float aMeasuredPressure );
    
    void setOneHitOffsetS( float aOffsetS ) { oneHitOffsetS = aOffsetS; };
    void setOneHitReleaseS( float aReleaseS ) { oneHitReleaseS = aReleaseS; };
    void setOneHitAttackS( float aAttackS ) { oneHitAttackS = aAttackS; };
    void clear();
    
    XFadeSamplerSound* getSound() { return sound;};
    int getMidiNoteNumber() { return midiNote;};
    int getLayerNumber();
    float getVelocity() { return velocity;};
    bool isEnvInRelease() { return envIsInRelease;};
    
    void setKeyGroupParams( float aGain, float aPan, int aTuneSt, float aTuneCt, float aBevel );
	void setKeyGroup( KeyGroupDefinition* aKeyGroupDefinition );
    bool isPlayingKeyGroup(KeyGroupDefinition* aKeyGroup) { return keyGroup == aKeyGroup;};
	float getKeyGroupGain() { return keyGroupGain; };
	float getKeyGroupPan() { return keyGroupPan; };
	int getKeyGroupTuneSt() { return keyGroupTuneSt; };
	float getKeyGroupTune() { return keyGroupTune; };
	float getKeyGroupBevel() { return keyGroupBevel; };


    
    void setSoundingMidiNote( int aSoundingMidiNote ) { soundingMidiNote = aSoundingMidiNote;};
    
    
private:
    //==============================================================================
    ThreadPool* dfdThreadPool;
    PluginParameters* pluginParameters;
    SampleBin* sampleBin;
    XFadeSamplerSound* sound;
    float velocityGain;
    float soundLPanGain, soundRPanGain;
    float soundGain;
    float pluginGain;
    float pluginPan;
    float pluginLGain;
    float pluginRGain;
    
    int playingLayerNr;
    
    int nElements;
    std::vector<ElementPlayData> elementPlayData;
    
    int nrOfElementsPlaying;
    
    
    // ENVELOPE HANDLING
    EnvParameters envParameters;
    
    int envNrOfPoints;
    int envCurrentPointNdx ;
    float envGainNext;
    float envGainDelta;
    float envGain;
    float envSamples;
    int envReleaseSamples;
    float envReleaseLevel;
    float envReleaseDelta;
    bool envIsInRelease;
    //
    
    int midiNote;
    float velocity;
    bool pressureCatched;
    int appliedPressure;
    int firstElementToPlayNdx;
    int lastElementToPlayNdx;
    int pitchWheelTuneSt;
    
    float oneHitOffsetS;
    float oneHitReleaseS;
    float oneHitAttackS;

	
	KeyGroupDefinition*	keyGroup;
    float keyGroupGain;
    float keyGroupPan;
    int keyGroupTuneSt;
    float keyGroupTune;
	float keyGroupBevel;
	
	
    // DFD temp variables
    bool dfd;
    int dfdPreloadFrames;
    int dfdLoadAtFrames;
    int dfdLoadAtFramesLeft;
    int dfdLoadFrames;
    int dfdBufferTotalSize;
    int dfdLoad1AtFrames;
    int dfdLoad0AtFrames;
    DFDRingBuffer* tmpRingBuf;
    

    // Tmp variables
    float el0;
    float er0;
    float el1;
    float er1;
    float* outL;
    float* outR;
    int samplePos;
    int pos;
    int posPlusOne;
    float alpha;
    float invAlpha;
    float r;
    float l;
    float summedLGain;
    float summedRGain;
	int framesLeft;
	int framesToLoad;
	int fromFrame;
	int ndxInBuffer;
    
    JUCE_LEAK_DETECTOR (XFadeSamplerVoice)
};





#endif /* XFadeSamplerVoice_hpp */
