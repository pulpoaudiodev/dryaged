/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchCreator.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#include "Configurator.hpp"
#include "../ProductDef.h"
#include "UIControls.hpp"



Configurator::Configurator()
{
}






bool Configurator::configurePatchBankFromXML(  std::string& aXML , PatchBank& patchBank)
{
    ScopedPointer<XmlElement> xmlDoc = XmlDocument::parse( aXML );
    //Logger::getCurrentLogger()->writeToLog("createPatchesFromXML");
    
    if( xmlDoc->hasTagName( "Instrument" )) {
        //Logger::getCurrentLogger()->writeToLog("Top Node " + xmlDoc->getTagName());
        
        XmlElement* xmlEl = xmlDoc->getFirstChildElement();
        while( xmlEl != nullptr ) {
            //Logger::getCurrentLogger()->writeToLog("2nd Node " + xmlEl->getTagName());
            if( xmlEl->hasTagName("Sounds")) {
                createSoundDefinitionsFromXML( xmlEl, patchBank);
            }
            if( xmlEl->hasTagName("Patches")) {
                createPatchDefinitionsFromXML( xmlEl, patchBank);
            }
            xmlEl = xmlEl->getNextElement();
        }
        return true;
    }

    return false;
}


void Configurator::createSoundDefinitionsFromXML( XmlElement* xmlSounds, PatchBank& patchBank )
{
    XmlElement* xmlSound = xmlSounds->getFirstChildElement();
    while( xmlSound ) {
        //Logger::getCurrentLogger()->writeToLog("3rd Node " + xmlPatch->getTagName());
        if( xmlSound->hasTagName("Sound")) {
            SoundDefinition soundDef;
            createSoundDefinitionFromXML( soundDef, xmlSound );
            patchBank.addSoundDefinition(soundDef);
        }
        xmlSound = xmlSound->getNextElement();
    }
    
}

void Configurator::createPatchDefinitionsFromXML( XmlElement* xmlPatches, PatchBank& patchBank )
{
    XmlElement* xmlPatch = xmlPatches->getFirstChildElement();
    while( xmlPatch ) {
        //Logger::getCurrentLogger()->writeToLog("3rd Node " + xmlPatch->getTagName());
        if( xmlPatch->hasTagName("Patch")) {
            PatchDefinition aPatchDefinition;
            createPatchDefinitionFromXML( aPatchDefinition, xmlPatch,patchBank );
            patchBank.addPatchDefinition(aPatchDefinition);
        }
        xmlPatch = xmlPatch->getNextElement();
    }
}




void Configurator::createPatchDefinitionFromXML( PatchDefinition& patchDef, XmlElement* xmlPatch, PatchBank& patchBank  )
{
    patchDef.setName( xmlPatch->getStringAttribute( "name" , "").toStdString() );
    
    
    int displaceSemiTones = xmlPatch->getIntAttribute( "displaceSemiTones",0 );
    int rangeFromKey = xmlPatch->getIntAttribute("rangeFromKey",0) + displaceSemiTones;
    int rangeToKey = xmlPatch->getIntAttribute("rangeToKey",127) + displaceSemiTones;
    patchDef.setPlayRange( rangeFromKey,rangeToKey );
    
    
    XmlElement* xmlSetup = xmlPatch->getChildByName("Setup");
    if( xmlSetup != nullptr) {
        patchDef.setNumberOfVoices( xmlSetup->getIntAttribute( "numVoices",8 ))	;
        patchDef.setGainDb( (float)xmlSetup->getDoubleAttribute( "gainDb",0.0 ));
        patchDef.setPan( (float)xmlSetup->getDoubleAttribute( "pan",0.0 ));
        patchDef.setTuneSt( xmlSetup->getIntAttribute( "tuneSt",0 ));
        patchDef.setTune((float)xmlSetup->getDoubleAttribute( "tune",0.0 ));
        patchDef.setVeloSens((float)xmlSetup->getDoubleAttribute( "veloSens",0.75 ));
        patchDef.setMonoMode( xmlSetup->getBoolAttribute( "monophonic", false));
    }
    
    XmlElement* xmlLayers = xmlPatch->getChildByName("Layers");
    if( xmlLayers != nullptr) {
        XmlElement* xmlLayer = xmlLayers->getFirstChildElement();
        while( xmlLayer != nullptr ) {
            if( xmlLayer->hasTagName("Layer")) {
                int layerNr = xmlLayer->getIntAttribute("nr",0);
                if( layerNr > -1 && layerNr < patchDef.layerGainDb.size()) {
                    patchDef.layerGainDb[layerNr] = (float)xmlLayer->getDoubleAttribute("gainDb",0.0);
                }
            }
            xmlLayer = xmlLayer->getNextElement();
        }
        
    }
    
    XmlElement* xmlPressure = xmlPatch->getChildByName("Pressure");
    if( xmlPressure != nullptr) {
        PatchDefinition::PRESSUREMODE pressMode = PatchDefinition::PRESSUREMODE::VELOCITY;
        std::string pressModeStr = xmlPressure->getStringAttribute( "mode" ).toStdString();
        if( pressModeStr == "PRESSUREABS" ) {
            pressMode = PatchDefinition::PRESSUREMODE::PRESSUREABS;
        } else if (pressModeStr == "VELOCITYPRESSUREABS"){
            pressMode = PatchDefinition::PRESSUREMODE::VELOCITYPRESSUREABS;
        }
        patchDef.setPressureMode( pressMode, xmlPressure->getIntAttribute( "controller",0 ));
    }
    
    XmlElement* xmlPitchwheel = xmlPatch->getChildByName("Pitchwheel");
    if( xmlPitchwheel != nullptr) {
        PatchDefinition::PITCHWHEELMODE pitchMode = PatchDefinition::PITCHWHEELMODE::NOPITCH;
        std::string pitchModeStr = xmlPitchwheel->getStringAttribute( "mode" ).toStdString();
        if( pitchModeStr == "SEMITONES" ) {
            pitchMode = PatchDefinition::PITCHWHEELMODE::SEMITONES;
        } else if( pitchModeStr == "SEMITONES_RETRIGGER" ) {
            pitchMode = PatchDefinition::PITCHWHEELMODE::SEMITONES_RETRIGGER;
        } else if( pitchModeStr == "CONTINUOUS") {
            pitchMode = PatchDefinition::PITCHWHEELMODE::CONTINUOUS;
        }
        patchDef.setPitchWheelDefinition( pitchMode,
                                          xmlPitchwheel->getIntAttribute( "rangeSt",12 ),
                                          (float)xmlPitchwheel->getDoubleAttribute("releaseTime",0.0),
                                          (float)xmlPitchwheel->getDoubleAttribute("offsetTime",0.0),
                                          (float)xmlPitchwheel->getDoubleAttribute("attackTime",0.0) );
    }
    
    XmlElement* xmlLegato = xmlPatch->getChildByName("Legato");
    if( xmlLegato != nullptr) {
        PatchDefinition::LEGATOMODE legatoMode = PatchDefinition::LEGATOMODE::NOLEGATO;
        std::string pitchModeStr = xmlLegato->getStringAttribute( "mode" ).toStdString();
        if( pitchModeStr == "SIMPLE" ) {
            legatoMode = PatchDefinition::LEGATOMODE::SIMPLE;
        }
        patchDef.setLegatoDefinition( legatoMode,
                                      (float)xmlLegato->getDoubleAttribute("releaseTime",0.0),
                                      (float)xmlLegato->getDoubleAttribute("offsetTime",0.0),
                                      (float)xmlLegato->getDoubleAttribute("attackTime",0.0) );
    }
    
    XmlElement* xmlDfd = xmlPatch->getChildByName("Dfd");
    if( xmlDfd != nullptr) {
        patchDef.setDfd( xmlDfd->getBoolAttribute("dfd",false),
                         xmlDfd->getIntAttribute("numRingBuffers",0),
                         xmlDfd->getIntAttribute("preloadFrames",44100),
                         xmlDfd->getIntAttribute("loadFrames",22050),
                         xmlDfd->getIntAttribute("loadAtFramesLeft",11025) );
    }
    
    XmlElement* xmlEnv = xmlPatch->getChildByName("AHDSREnvelope");
    if( xmlEnv != nullptr) {
        patchDef.setEnvelopeDefinition((float)xmlEnv->getDoubleAttribute("offsetTime",0.0),
                                        (float)xmlEnv->getDoubleAttribute("attackTime",0.0),
                                        (float)xmlEnv->getDoubleAttribute("holdTime",0.0),
                                        (float)xmlEnv->getDoubleAttribute("decayTime",0.0),
                                        (float)xmlEnv->getDoubleAttribute("sustainDb",0.0),
                                        (float)xmlEnv->getDoubleAttribute("releaseTime",0.0) );
    }
    
    XmlElement* xmlKeyswitches = xmlPatch->getChildByName("Keyswitches");
    if( xmlKeyswitches != nullptr) {
        patchDef.setKeyswitchRange( xmlKeyswitches->getIntAttribute("rangeFromKey",-1) + displaceSemiTones,
                                    xmlKeyswitches->getIntAttribute("rangeToKey",-1) +displaceSemiTones );
        
        XmlElement* xmlKeyswitch = xmlKeyswitches->getFirstChildElement();
        while( xmlKeyswitch != nullptr ) {
            if( xmlKeyswitch->hasTagName("Keyswitch") ) {
                
                KeyswitchDefinition::KEYSWITCHTYPE aType = KeyswitchDefinition::KEYSWITCHTYPE::KEYSWITCHTYPE_SOUNDSET;
                std::string aTypeStr = xmlKeyswitch->getStringAttribute("type","SOUNDSET").toStdString();
                if( aTypeStr == "SIMPLELEGATO") {
                    aType = KeyswitchDefinition::KEYSWITCHTYPE::KEYSWITCHTYPE_SIMPLELEGATO;
                }
                
                patchDef.addKeyswitchDefinition( xmlKeyswitch->getStringAttribute("name","").toStdString(),
                                                 xmlKeyswitch->getIntAttribute("key",-1),
                                                 xmlKeyswitch->getBoolAttribute("permanent",false),
                                                 aType,
                                                 xmlKeyswitch->getIntAttribute("parameter",-1));
            }
            xmlKeyswitch = 	xmlKeyswitch->getNextElement();
        }
    }
    
    
    XmlElement* xmlKeyGroups = xmlPatch->getChildByName("KeyGroups");
    if( xmlKeyGroups != nullptr) {
        XmlElement* xmlKeyGroup = xmlKeyGroups->getFirstChildElement();
        while( xmlKeyGroup != nullptr ) {
            if( xmlKeyGroup->hasTagName("KeyGroup") ) {
                patchDef.addKeyGroup( xmlKeyGroup->getStringAttribute("name","").toStdString(),
                                      xmlKeyGroup->getBoolAttribute("mono",false),
                                      xmlKeyGroup->getIntAttribute("rangeFromKey",0) + displaceSemiTones,
                                      xmlKeyGroup->getIntAttribute("rangeToKey",127) + displaceSemiTones,
                                      (float)xmlKeyGroup->getDoubleAttribute("gainDb",0.0),
                                      (float)xmlKeyGroup->getDoubleAttribute("pan",0.0),
                                      xmlKeyGroup->getIntAttribute("tuneSt",0),
                                      (float)xmlKeyGroup->getDoubleAttribute("tuneCt",0.0),
                                      (float)xmlKeyGroup->getDoubleAttribute("bevel",0.0)
                                      );
            }
            xmlKeyGroup = 	xmlKeyGroup->getNextElement();
        }
    }
    
    
    
    XmlElement* xmlSounds = xmlPatch->getChildByName("Sounds");
    if( xmlSounds != nullptr) {
        XmlElement* xmlSound = xmlSounds->getFirstChildElement();
        while( xmlSound != nullptr ) {
            if( xmlSound->hasTagName("Sound") ) {
                std::string aSoundName = xmlSound->getStringAttribute("name","").toStdString();
                SoundDefinition* aSoundDef = patchBank.getSoundDefinitionAt(aSoundName);
                if( aSoundDef != nullptr) {
                    SoundDefinition* newSd = new SoundDefinition();
                    *newSd = *aSoundDef;   //make a copy
                    
                    newSd->set(
                               xmlSound->getIntAttribute("noteRangeFrom",-1)+displaceSemiTones,
                               xmlSound->getIntAttribute("noteRangeTo",-1)+displaceSemiTones,
                               xmlSound->getIntAttribute("centerNote",-1)+displaceSemiTones,
                               (float)xmlSound->getDoubleAttribute("gainDb",0.0),
                               xmlSound->getIntAttribute("tuneSt",0),
                               (float)xmlSound->getDoubleAttribute("tuneCt",0.0),
                               (float)xmlSound->getDoubleAttribute("pan",0.0),
                               xmlSound->getIntAttribute("layer",0),
                               xmlSound->getIntAttribute("soundSet",0),
                               xmlSound->getBoolAttribute("oneShot", false) );
                    
                    patchDef.addSoundDefinition( *newSd );
                }
                
            }
            xmlSound = xmlSound->getNextElement();
        }
    }
    
    
    /*XmlElement* xmlSounds = xmlPatch->getChildByName("Sounds");
     if( xmlSounds != nullptr) {
     patchDef.setPlayRange( xmlSounds->getIntAttribute("rangeFromKey",0) + displaceSemiTones,
     xmlSounds->getIntAttribute("rangeToKey",127) + displaceSemiTones);
     
     
     XmlElement* xmlSound = xmlSounds->getFirstChildElement();
     while( xmlSound != nullptr ) {
     if( xmlSound->hasTagName("Sound") ) {
     SoundDefinition soundDef( xmlSound->getStringAttribute("name","").toStdString(),
     xmlSound->getIntAttribute("noteRangeFrom",-1)+displaceSemiTones,
     xmlSound->getIntAttribute("noteRangeTo",-1)+displaceSemiTones,
     xmlSound->getIntAttribute("centerNote",-1)+displaceSemiTones,
     xmlSound->getBoolAttribute("xFade",false),
     (float)xmlSound->getDoubleAttribute("gainDb",0.0),
     xmlSound->getIntAttribute("tuneSt",0),
     (float)xmlSound->getDoubleAttribute("tuneCt",0.0),
     (float)xmlSound->getDoubleAttribute("pan",0.0),
     xmlSound->getIntAttribute("layer",0),
     xmlSound->getIntAttribute("soundSet",0),
     xmlSound->getBoolAttribute("oneShot", false) );
     
     
     // Velocity auto distribution setup
     int nrOfVelocitiesToDistribute = xmlSound->getIntAttribute("velLayers",0);
     double veloPerLayer = 0.0;
     if( nrOfVelocitiesToDistribute > 0) {
     veloPerLayer = 1.0 / nrOfVelocitiesToDistribute;
     }
     int veloNdx = 0;
     
     XmlElement* xmlSoundElement = xmlSound->getFirstChildElement();
     while( xmlSoundElement != nullptr ) {
     if( xmlSoundElement->hasTagName("SoundElement") ) {
     
     double veloRangeFrom = xmlSoundElement->getDoubleAttribute("velocityRangeFrom",0.0);
     double veloRangeTo = xmlSoundElement->getDoubleAttribute("velocityRangeTo",0.0);
     double xFadeVeloTo = xmlSoundElement->getDoubleAttribute("xFadeVelocityTo",0.0);
     
     // Velocity auto distribution
     if( nrOfVelocitiesToDistribute > 0) {
     veloRangeFrom = veloPerLayer * veloNdx;
     veloRangeTo = veloPerLayer * (veloNdx+1);
     xFadeVeloTo = veloPerLayer * (veloNdx+2);
     if( xFadeVeloTo > 1.0 ) xFadeVeloTo = 1.0;
     }
     
     
     SoundElementDefinition selDef((float)xmlSoundElement->getDoubleAttribute("gainDb",0.0),
     (float)veloRangeFrom,
     (float)veloRangeTo,
     (float)xFadeVeloTo);
     
     SoundElementDefinition::ROUNDROBINMODE aRRMode = SoundElementDefinition::ROUNDROBINMODE::OFF;
     std::string aRRModeStr = xmlSoundElement->getStringAttribute("roundRobinMode","OFF").toStdString();
     if( aRRModeStr == "CYCLE") {
     aRRMode = SoundElementDefinition::ROUNDROBINMODE::CYCLE;
     } else if( aRRModeStr == "RANDOM") {
     aRRMode = SoundElementDefinition::ROUNDROBINMODE::RANDOM;
     }
     selDef.setRoundRobinMode( aRRMode );
     
     if( xmlSoundElement->hasAttribute("sample")) {
     std::string aSampleName = xmlSoundElement->getStringAttribute("sample","").toStdString();
     if( aSampleName != "" ) {
     selDef.addRoundRobinSound( aSampleName );
     }
     
     }
     
     XmlElement* xmlRRSample = xmlSoundElement->getFirstChildElement();
     while( xmlRRSample != nullptr ) {
     if( xmlRRSample->hasTagName("Sample") ) {
     std::string aSampleName = xmlRRSample->getStringAttribute("name","").toStdString();
     if( aSampleName != "" ) {
     selDef.addRoundRobinSound( aSampleName );
     }
     }
     xmlRRSample = xmlRRSample->getNextElement();
     }
     soundDef.addSoundElementDefinition( selDef );
     }
     
     xmlSoundElement = xmlSoundElement->getNextElement();
     veloNdx++;
     }
     
     patchDef.addSoundDefinition( soundDef );
     }
     xmlSound = xmlSound->getNextElement();
     }
     }*/
}















void Configurator::createSoundDefinitionFromXML( SoundDefinition& soundDef, XmlElement* xmlSound )
{
    /*xmlSound->getIntAttribute("noteRangeFrom",-1)+displaceSemiTones,
     xmlSound->getIntAttribute("noteRangeTo",-1)+displaceSemiTones,
     xmlSound->getIntAttribute("centerNote",-1)+displaceSemiTones,
     (float)xmlSound->getDoubleAttribute("gainDb",0.0),
     xmlSound->getIntAttribute("tuneSt",0),
     (float)xmlSound->getDoubleAttribute("tuneCt",0.0),
     (float)xmlSound->getDoubleAttribute("pan",0.0),
     xmlSound->getIntAttribute("layer",0),
     xmlSound->getIntAttribute("soundSet",0),
     );
     
     */
    soundDef.set( xmlSound->getStringAttribute("name","").toStdString(),
                  xmlSound->getBoolAttribute("xFade",false));
    
    
    
    // Velocity auto distribution setup
    int nrOfVelocitiesToDistribute = xmlSound->getIntAttribute("velLayers",0);
    double veloPerLayer = 0.0;
    if( nrOfVelocitiesToDistribute > 0) {
        veloPerLayer = 1.0 / nrOfVelocitiesToDistribute;
    }
    int veloNdx = 0;
    
    XmlElement* xmlSoundElement = xmlSound->getFirstChildElement();
    while( xmlSoundElement != nullptr ) {
        if( xmlSoundElement->hasTagName("SoundElement") ) {
            
            double veloRangeFrom = xmlSoundElement->getDoubleAttribute("velocityRangeFrom",0.0);
            double veloRangeTo = xmlSoundElement->getDoubleAttribute("velocityRangeTo",0.0);
            double xFadeVeloTo = xmlSoundElement->getDoubleAttribute("xFadeVelocityTo",0.0);
            
            // Velocity auto distribution
            if( nrOfVelocitiesToDistribute > 0) {
                veloRangeFrom = veloPerLayer * veloNdx;
                veloRangeTo = veloPerLayer * (veloNdx+1);
                xFadeVeloTo = veloPerLayer * (veloNdx+2);
                if( xFadeVeloTo > 1.0 ) xFadeVeloTo = 1.0;
            }
            
            
            SoundElementDefinition selDef;
            selDef.set(
                        (float)xmlSoundElement->getDoubleAttribute("gainDb",0.0),
                        (float)veloRangeFrom,
                        (float)veloRangeTo,
                        (float)xFadeVeloTo);
            
            SoundElementDefinition::ROUNDROBINMODE aRRMode = SoundElementDefinition::ROUNDROBINMODE::OFF;
            std::string aRRModeStr = xmlSoundElement->getStringAttribute("roundRobinType","OFF").toStdString();
            if( aRRModeStr == "CYCLE") {
                aRRMode = SoundElementDefinition::ROUNDROBINMODE::CYCLE;
            } else if( aRRModeStr == "RANDOM") {
                aRRMode = SoundElementDefinition::ROUNDROBINMODE::RANDOM;
            }
            selDef.setRoundRobinMode( aRRMode );
            
            if( xmlSoundElement->hasAttribute("sample")) {
                std::string aSampleName = xmlSoundElement->getStringAttribute("sample","").toStdString();
                if( aSampleName != "" ) {
                    selDef.addRoundRobinSound( aSampleName );
                }
                
            }
            
            XmlElement* xmlRRSample = xmlSoundElement->getFirstChildElement();
            while( xmlRRSample != nullptr ) {
                if( xmlRRSample->hasTagName("Sample") ) {
                    std::string aSampleName = xmlRRSample->getStringAttribute("name","").toStdString();
                    if( aSampleName != "" ) {
                        selDef.addRoundRobinSound( aSampleName );
                    }
                }
                xmlRRSample = xmlRRSample->getNextElement();
            }
            soundDef.addSoundElementDefinition( selDef );
        }
        
        xmlSoundElement = xmlSoundElement->getNextElement();
        veloNdx++;
    }
}












bool Configurator::configureUIElementsFromXML( std::string& aXML, SampleBin* aSampleBin, UIConfig& uiConfig)
{
    ScopedPointer<XmlElement> xmlDoc = XmlDocument::parse( aXML );
    //Logger::getCurrentLogger()->writeToLog("createPatchesFromXML");
    
    if( xmlDoc->hasTagName( "Instrument" )) {
        //Logger::getCurrentLogger()->writeToLog("Top Node " + xmlDoc->getTagName());
        
        XmlElement* xmlEl = xmlDoc->getFirstChildElement();
        while( xmlEl != nullptr ) {
            //Logger::getCurrentLogger()->writeToLog("2nd Node " + xmlEl->getTagName());
			if( xmlEl->hasTagName("Infos")) {
                XmlElement* xmlInfoElement = xmlEl->getFirstChildElement();
                if( xmlInfoElement->hasTagName("Licence")) {
                    std::string licenceText;
                    XmlElement* xmlLicenceElement =xmlInfoElement->getFirstChildElement();
                    while( xmlLicenceElement ) {
                        if( xmlLicenceElement->hasTagName("Line")) {
                            licenceText += xmlLicenceElement->getAllSubText().toStdString() + "\n";
                        }
                        xmlLicenceElement = xmlLicenceElement->getNextElement();
                    }
                    uiConfig.licenceText = licenceText;
                }
            } else if( xmlEl->hasTagName("UI") ) {
                uiConfig.w = xmlEl->getIntAttribute("w",640);
                uiConfig.h = xmlEl->getIntAttribute("h",400);
                uiConfig.showBackground = xmlEl->getBoolAttribute("showBackground",true);
                uiConfig.showReloadPatchesButton = xmlEl->getBoolAttribute("showReloadPatchesButton",false);
                uiConfig.showReloadUIButton = xmlEl->getBoolAttribute("showReloadUIButton",false);
                
                
                XmlElement* xmlUiElement = xmlEl->getFirstChildElement();
                while( xmlUiElement ) {
                    //Logger::getCurrentLogger()->writeToLog("3rd Node " + xmlUiElement->getTagName());
                    if( xmlUiElement->hasTagName("Element")) {
                        UIElementConfig elementConfig = createElementConfigFromXML( xmlUiElement );
                        if( elementConfig.name != "") {
                            uiConfig.addElementConfig(elementConfig);
                        }
                    } else if(xmlUiElement->hasTagName("About")) {
                        uiConfig.aboutX=xmlUiElement->getIntAttribute("x",0);
                        uiConfig.aboutY=xmlUiElement->getIntAttribute("y",0);
                        uiConfig.aboutW=xmlUiElement->getIntAttribute("w",0);
                        uiConfig.aboutH=xmlUiElement->getIntAttribute("h",0);
                    } else if( xmlUiElement->hasTagName("LookAndFeels")) {
                        XmlElement* xmlLaFElement = xmlUiElement->getFirstChildElement();
                        while( xmlLaFElement ) {
                            if( xmlLaFElement->hasTagName("LookAndFeel")) {
                                
                                MyLookAndFeel* laF = new MyLookAndFeel();
                                std::string lafName = xmlLaFElement->getStringAttribute("name","default").toStdString();
                                
                                int aRotarySliderWidth = xmlLaFElement->getIntAttribute("rotarySliderWidth", 0);
                                int aRotarySliderHeight = xmlLaFElement->getIntAttribute("rotarySliderHeight", 0);
                                int aToggleButtonWidth = xmlLaFElement->getIntAttribute("toggleButtonWidth", 0);
                                int aToggleButtonHeight = xmlLaFElement->getIntAttribute("toggleButtonWidth", 0);

                                Colour aComboBoxBackgroundColour=Colour( xmlLaFElement->getStringAttribute("comboBoxBackgroundColour","0xa0cbb78a").getHexValue32() );
                                Colour aPopupMenuBackgroundColour=Colour( xmlLaFElement->getStringAttribute("popupMenuBackgroundColour","0xa0cbb78a").getHexValue32() );
                                Colour aTextButtonColour=Colour( xmlLaFElement->getStringAttribute("textButtonColour","0xa0cbb78a").getHexValue32() );
                                
                                Colour aTextBoxBackgroundColour=Colour( xmlLaFElement->getStringAttribute("textBoxBackgroundColour","0x1000200").getHexValue32() );
                                Colour aTextBoxTextColour=Colour( xmlLaFElement->getStringAttribute("textBoxTextColour","0x1000201").getHexValue32() );
                                Colour aTextBoxTextEditColour=Colour( xmlLaFElement->getStringAttribute("textBoxTextEditColour","0xff000000").getHexValue32() );
                                Colour aTextBoxHighlightColour=Colour( xmlLaFElement->getStringAttribute("textBoxHighlightColour","0x1000202").getHexValue32() );
                                Colour aTextBoxHighlightedTextColour=Colour( xmlLaFElement->getStringAttribute("textBoxHighlightedTextColour","0x1000203").getHexValue32() );
                                Colour aTextBoxOutLineColour=Colour( xmlLaFElement->getStringAttribute("textBoxOutlineColour","0x1000205").getHexValue32() );
                                Colour aTextBoxFocusedOutLineColour=Colour( xmlLaFElement->getStringAttribute("textBoxFocusedOutlineColour","0x1000206").getHexValue32() );
                                Colour aTextBoxShadowColour=Colour( xmlLaFElement->getStringAttribute("textBoxShadowColour","0x1000207").getHexValue32() );
                                
                                
                                std::string aKnobImageName = xmlLaFElement->getStringAttribute("sliderImage","").toStdString();
                                std::string aToggleImageName = xmlLaFElement->getStringAttribute("toggleImage","").toStdString();
                                
                                juce::Image* knobImage = aSampleBin->getImage(aKnobImageName);
                                if( knobImage != nullptr && aRotarySliderWidth > 0 && aRotarySliderHeight > 0 && knobImage->getHeight() > 0) {
                                    int count = knobImage->getHeight() / aRotarySliderHeight;
                                    laF->setRotarySliderKnobManImage(knobImage, aRotarySliderWidth, aRotarySliderHeight, count);
                                    
                                }
                                juce::Image* toggleImage = aSampleBin->getImage(aToggleImageName);
                                if( toggleImage != nullptr && aToggleButtonWidth > 0 && aToggleButtonHeight > 0 && toggleImage->getHeight() > 0) {
                                    int count = toggleImage->getHeight() / aToggleButtonHeight;
                                    laF->setToggleButtonKnobManImage(toggleImage, aToggleButtonWidth, aToggleButtonHeight, count);
                                }
                                
                                laF->setColours(
                                                aTextBoxBackgroundColour,
                                                aTextBoxTextColour,
                                                aTextBoxTextEditColour,
                                                aTextBoxHighlightColour,
                                                aTextBoxHighlightedTextColour,
                                                aTextBoxOutLineColour,
                                                aTextBoxFocusedOutLineColour,
                                                aTextBoxShadowColour,
                                                aComboBoxBackgroundColour,
                                                aPopupMenuBackgroundColour,
                                                aTextButtonColour
                                                );
                                                
                                
                                uiConfig.addLookAndFeel(lafName, laF);
                            }
                            xmlLaFElement = xmlLaFElement->getNextElement();
                        }
                        
                    }
                    xmlUiElement = xmlUiElement->getNextElement();
                }
                
            }
            xmlEl = xmlEl->getNextElement();
        }
        return true;
    }

    return false;
}

UIElementConfig Configurator::createElementConfigFromXML( XmlElement* xmlUiElement )
{
    UIElementConfig uec;
    if( xmlUiElement->hasTagName("Element") ) {
        uec.name = xmlUiElement->getStringAttribute( "name","" ).toStdString();
        uec.x = xmlUiElement->getIntAttribute("x",-1);
        uec.y = xmlUiElement->getIntAttribute("y",-1);
        uec.w = xmlUiElement->getIntAttribute("w",-1);
        uec.h = xmlUiElement->getIntAttribute("h",-1);
        uec.sizeX = xmlUiElement->getIntAttribute("sizeX",-1);
        uec.sizeY = xmlUiElement->getIntAttribute("sizeY",-1);
        uec.param = xmlUiElement->getStringAttribute( "param","" ).toStdString();
        uec.visible = xmlUiElement->getBoolAttribute("visible",false);
        uec.showTextBox = xmlUiElement->getBoolAttribute("showTextBox",false);
        uec.lookAndFeel = xmlUiElement->getStringAttribute( "lookAndFeel","" ).toStdString();
    }
    return uec;

}







