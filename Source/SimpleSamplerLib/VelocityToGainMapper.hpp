/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  VelocityToGainMapper.hpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 23/09/16.
//
//

#ifndef VelocityToGainMapper_hpp
#define VelocityToGainMapper_hpp

#include <stdio.h>





class VelocityToGainMapper
{
protected:
    
    
public:
    
    static float velocityToGain( float iSensitivity, float iVelocity);
    //static float applyBevel( int keyRangeFrom, int keyRangeTo, int key, float bevel, float iVelocity);
    static float bevelToGain( int keyRangeFrom, int keyRangeTo, int key, float bevel);
    
};


#endif /* VelocityToGainMapper_hpp */
