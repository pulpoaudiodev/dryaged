/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchDefinition.hpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#ifndef PatchDefinition_hpp
#define PatchDefinition_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include "JuceHeader.h"



/*--------------------------------------------------------------------------------------------------------------
 * SoundElementDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

struct SoundElementDefinition {
public:
	enum ROUNDROBINMODE {
		OFF = 0,
		CYCLE,
		RANDOM
	};

	std::vector<std::string> soundElementRRNames;
	float velocityRangeFrom;
	float velocityRangeTo;
	float xFadeVelocityTo;
	float gainDb;
	ROUNDROBINMODE roundRobinMode;


	SoundElementDefinition()
	{
		velocityRangeFrom = -1;
		velocityRangeTo = -1;
		xFadeVelocityTo = -1;
		gainDb = 0.0;
		roundRobinMode = ROUNDROBINMODE::OFF;
	}

    void set ( float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
    {
    velocityRangeFrom = aVelocityRangeFrom;
    velocityRangeTo = aVelocityRangeTo;
    xFadeVelocityTo = aXFadeVelocityTo;
    gainDb = aGainDb;
    roundRobinMode = ROUNDROBINMODE::OFF;
    }
    
    
    void setRoundRobinMode( ROUNDROBINMODE aRoundRobinMode) { roundRobinMode = aRoundRobinMode;};

	void addRoundRobinSound( std::string aRRSoundName ) {	soundElementRRNames.push_back( aRRSoundName );	};

	int getNrOfRoundRobinSounds() {
		if( roundRobinMode == ROUNDROBINMODE::OFF && soundElementRRNames.size() > 0) {
			return 1;
		} else {
			return soundElementRRNames.size();
		}
	}

	std::string* getRoundRobinSound( int aRRNdx ) {
		if( aRRNdx > -1 && aRRNdx < (int)soundElementRRNames.size()) {
			return &(soundElementRRNames[aRRNdx]);
		} else {
			return NULL;
		}
	}



};



/*--------------------------------------------------------------------------------------------------------------
 * SoundDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
class SoundDefinition
{
public:
	std::vector<SoundElementDefinition> soundElementDefinitions;
	std::string soundName;
	int noteRangeFrom;
	int noteRangeTo;
	int centerNote;
	bool xFade;
	float gainDb;
	float tuneCt;
    int tuneSt;
	float pan;
	int soundSet;
	int layer;
    bool oneShot;

	/*SoundDefinition( std::string aSoundName,
			int aNoteRangeFrom,
			int aNoteRangeTo,
			int aCenterNote,
			bool aXFade,
			float aGGainDb ,
            int aTuneSt,
			float aTuneCt ,
			float aPan ,
			int aLayer ,
			int aSoundSet,
            bool aOneShot);*/

	SoundDefinition();
    
    void set( std::string soundName,
                bool xFade);
    
	void set(
			int noteRangeFrom,
			int noteRangeTo,
			int centerNote,
			float aGainDb ,
            int aTuneSt,
			float aTuneCt ,
			float aPan ,
			int aLayer ,
			int aSoundSet,
            bool aOneShot);



	void addSoundElementDefinition( SoundElementDefinition& aSoundElementDefinition);
	/*void addSoundElementDefinition( std::string aSoundName, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo );

	void addSoundElementDefinition( std::string aSoundName, int xFadeSlot, int nXFadeSlots );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, int xFadeSlot, int nXFadeSlots );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, int xFadeSlot, int nXFadeSlots );
	void addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, int xFadeSlot, int nXFadeSlots );*/

};





/*--------------------------------------------------------------------------------------------------------------
 * AHDSREnvelopeDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class AHDSREnvelopeDefinition  {
public:
	float offsetT;
	float attackT;
	float holdT;
	float decayT;
	float sustainDb;
	float releaseT;

public:
	AHDSREnvelopeDefinition(float aOffsetT,
			float aAttackT,
			float aHoldT,
			float aDecayT,
			float aSustainDb,
			float aReleaseT);

	AHDSREnvelopeDefinition();

	virtual ~AHDSREnvelopeDefinition() {};

	void set (float aOffsetT,
			float aAttackT,
			float aHoldT,
			float aDecayT,
			float aSustainDb,
			float aReleaseT);

	float getOffsetT() {return offsetT;};
	float getAttackT() {return attackT;};
	float getHoldT() {return holdT;};
	float getDecayT() {return decayT;};
	float getSustainDb() {return sustainDb;};
	float getReleaseT() {return releaseT;};
};


/*--------------------------------------------------------------------------------------------------------------
 * KeyswitchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class KeyswitchDefinition
{
public:
	enum KEYSWITCHTYPE {
		KEYSWITCHTYPE_SOUNDSET = 0,
		KEYSWITCHTYPE_SIMPLELEGATO
	};


	KeyswitchDefinition( std::string aName, int aKey, bool aIsPermanent, KEYSWITCHTYPE aType, int aParam);
	std::string name;
	int key;
	KEYSWITCHTYPE type;
	int param;
	bool isPermanent;
};


/*--------------------------------------------------------------------------------------------------------------
 * KeyGroupDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class KeyGroupDefinition
{
public:
    std::string name;
    bool isMono;
    int fromKey;
    int toKey;
    float gainDb;
    float pan;
    int tuneSt;
    float tuneCt;
    float bevel;

	KeyGroupDefinition();
    KeyGroupDefinition( std::string aName, bool aIsMono, int aFromKey, int aToKey, float aGainDb, float aPan, int aTuneSt, float aTuneCt, float aBevel);
};


/*--------------------------------------------------------------------------------------------------------------
 * PatchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class PatchDefinition
{
public:

	enum PRESSUREMODE {
		VELOCITY = 0,
		PRESSUREABS,
		VELOCITYPRESSUREABS
	};

	enum PITCHWHEELMODE {
        NOPITCH = 0,
		CONTINUOUS,
		SEMITONES,
		SEMITONES_RETRIGGER
	};

	enum LEGATOMODE {
		NOLEGATO = 0,
		SIMPLE
	};

public:

	int numberOfVoices;
	std::string name;
	//std::vector<SoundDefinition> soundDefinitions;
    std::vector<SoundDefinition> soundDefinitions;
    std::vector<std::string> soundDefinitionNames;
    AHDSREnvelopeDefinition envelopeDefinition;
	float gainDb;
	float pan;
	float tune;
    int tuneSt;
	float veloSens;
    std::vector<float> layerGainDb;


	int keySwitchRangeFromMidiNote;
	int keySwitchRangeToMidiNote;
	int playRangeFromMidiNote;
	int playRangeToMidiNote;
	std::vector<KeyswitchDefinition> keyswitchDefinitions;

	PRESSUREMODE pressureMode;
	int pressureController;

	PITCHWHEELMODE pitchWheelMode;
	int pitchWheelRangeSt;
	float pitchWheelRetriggerReleaseS;
	float pitchWheelRetriggerOffsetS;
	float pitchWheelRetriggerAttackS;

	LEGATOMODE legatoMode;
	float legatoReleaseS;
	float legatoOffsetS;
	float legatoAttackS;

	bool monoMode;

	bool dfd;
	int dfdPreloadFrames;
	int dfdNrOfRingBuffers;
	int dfdLoadFrames;
	int dfdLoadAtFramesLeft;

    std::vector<KeyGroupDefinition> keyGroups;

	StringArray soundSetNames;

public:

	//PatchDefinition(std::string aName);
	PatchDefinition();
	~PatchDefinition();

	void init();

	void setName( std::string aName);
	std::string& getName() {return name;};

	void setNumberOfVoices( int aNumberOfVoices ) { numberOfVoices = aNumberOfVoices;};
    
    void addSoundDefinitionName( std::string& aName );
    int getNrOfSoundDefinitionNames();
    std::string& getSoundDefinitionNameAt( int ndx );
    void addSoundDefinition( SoundDefinition& aSoundDefinition);
    int getNrOfSoundDefinitions();
    SoundDefinition* getSoundDefinitionAt( int ndx );
    
	/*void addSoundDefinition( SoundDefinition& aSoundDefinition);
	SoundDefinition* createAndAddSoundDefinition();

	int getNrOfSoundDefinitions();
	SoundDefinition& getSoundDefinitionAt( int ndx );*/
    
    
	AHDSREnvelopeDefinition& getEnvelopeDefinition();

	void setVeloSens( float aVeloSens) { veloSens = aVeloSens;};
	void setEnvelopeDefinition(AHDSREnvelopeDefinition& aEnvelopeDefinition);
	void setEnvelopeDefinition(float aOffsetT, float aAttackT, float aHoldT, float aDecayT, float aSustainDb, float aReleaseT);

	int getNumberOfVoices() { return numberOfVoices;};
	float getGainDb() { return gainDb;};
    int getTuneSt() { return tuneSt; };
	float getTune() { return tune; };
	float getPan() { return pan;};
	float getVeloSens() { return veloSens;};
    
    float getLayerGainDb( int aLayerNr );
    void setLayerGainDb( int aLayerNr, float aGainDb);
    
	/*float getLayer1GainDb() { return layer1GainDb; };
	float getLayer2GainDb() { return layer2GainDb; };
	float getLayer3GainDb() { return layer3GainDb; };
	float getLayer4GainDb() { return layer4GainDb; };*/

	void setGainDb( float aGainDb ) { gainDb = aGainDb; };
	void setTune( float aTune ) { tune = aTune; };
    void setTuneSt( int aTuneSt ) { tuneSt = aTuneSt; };
	void setPan( float aPan ) { pan = aPan;};
	/*void setLayer1GainDb( float aLayerDb) { layer1GainDb = aLayerDb; };
	void setLayer2GainDb( float aLayerDb) { layer3GainDb = aLayerDb; };
	void setLayer3GainDb( float aLayerDb) { layer4GainDb = aLayerDb; };
	void setLayer4GainDb( float aLayerDb) { layer4GainDb = aLayerDb; };*/


	void setKeyswitchRange( int fromMidiNoteNr, int toMidiNoteNr) { keySwitchRangeFromMidiNote = fromMidiNoteNr; keySwitchRangeToMidiNote = toMidiNoteNr; };
	void setPlayRange( int fromMidiNoteNr, int toMidiNoteNr) { playRangeFromMidiNote = fromMidiNoteNr; playRangeToMidiNote = toMidiNoteNr; };
	void addKeyswitchDefinition( KeyswitchDefinition& aKeyswitch);
	void addKeyswitchDefinition( std::string aName, int aMidiNote, bool aIsPermanent, KeyswitchDefinition::KEYSWITCHTYPE aType, int aParam);
	int getKeySwitchRangeFromMidiNote() { return keySwitchRangeFromMidiNote;};
	int getKeySwitchRangeToMidiNote() { return keySwitchRangeToMidiNote;};
	std::vector<KeyswitchDefinition>& getKeyswitchDefinitions() { return keyswitchDefinitions; };

	void setPressureMode( PRESSUREMODE aPressureMode, int aPressureController) { pressureMode = aPressureMode; pressureController = aPressureController;};
	PRESSUREMODE getPressureMode() { return pressureMode;};
	int getPressureController() { return pressureController;};

	void setPitchWheelDefinition( PITCHWHEELMODE aPitchWheelMode, int aPitchWheelRangeSt, float aRetriggerReleaseS, float aRetriggerOffsetS, float aRetriggerAttackS );
	int getPitchWheelMode() { return pitchWheelMode;};
	int getPitchWheelRangeSt() { return pitchWheelRangeSt;};
	float getPitchWheelRetriggerReleaseS() { return pitchWheelRetriggerReleaseS;};
	float getPitchWheelRetriggerOffsetS() { return pitchWheelRetriggerOffsetS;};
	float getPitchWheelRetriggerAttackS() { return pitchWheelRetriggerAttackS;};

	void setLegatoDefinition( LEGATOMODE aLegatoMode, float aLegatoReleaseS, float aLegatoOffsetS, float aLegatoAttackS );
	int getLegatoMode() { return legatoMode;};
	float getLegatoReleaseS() { return legatoReleaseS;};
	float getLegatoOffsetS() { return legatoOffsetS;};
	float getLegatoAttackS() { return legatoAttackS;};

	void setMonoMode( bool aMonoMode ) { monoMode = aMonoMode;};
	bool isMonoMode() { return monoMode;};

	void setDfd( bool aDfd, int aNrOfRingBuffers, int aDfdPreloadFrames, int aDfdLoadFrames, int aDfdLoadAtFramesLeft);
	bool getDfd() { return dfd;};
	int getDfdPreloadFrames() { return dfdPreloadFrames;};
	int getDfdLoadFrames() { return dfdLoadFrames;};
    int getDfdLoadAtFramesLeft() { return dfdLoadAtFramesLeft;};
	int getNrOfRingBuffers() { return dfdNrOfRingBuffers;};

	void addMonoGroupRange( int fromNoteNr, int toNoteNr );
    
    void addKeyGroup( std::string aName, bool aIsMono, int aFromKey, int aToKey, float aRelGainDb, float aRelPan, int aRelTuneSt, float aRelTuneCt, float aBevel);
	std::vector<KeyGroupDefinition>& getKeyGroupDefinitions() { return keyGroups;};

	StringArray& getSoundSetNames() { return soundSetNames;};

};


class PatchBank {
private:
    std::vector<PatchDefinition> patchDefinitionsVector;
    std::map<std::string, SoundDefinition> soundDefinitionsMap;
    
public:
    void clear();

    void addPatchDefinition( PatchDefinition& aPatchDefinition);
    PatchDefinition* getPatchDefinitionAt( std::string& aName);
    PatchDefinition* getPatchDefinitionAt( int aNdx);
    int getPatchDefinitionNdx( std::string& aName);
    int getNrOfPatchDefinitions();
    
    void addSoundDefinition( SoundDefinition& aSoundDefinition);
    SoundDefinition* getSoundDefinitionAt( std::string& aName);


    
};



#endif /* PatchDefinition_hpp */
