/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  SampleBin.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 08/11/16.
//
//

#include "SampleBin.hpp"

#include <sstream>
#include "BinAudioFormat.hpp"
#include "SamplerErrors.hpp"
//#include "UIImages.h"


/*--------------------------------------------------------------------------------------------------------------
 * DFDRingBuffer
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

DFDRingBuffer::DFDRingBuffer( int aNumChannels, int aNumberOfFrames )
{
	available = true;
    inL = NULL;
    inR = NULL;
    resize( aNumChannels, aNumberOfFrames );
   
    reset();
};

DFDRingBuffer::~DFDRingBuffer()
{
    reset();
}


void DFDRingBuffer::reset()
{
	buf0Ready = false;
    buf1Ready = false;
    buf0Requested= false;
    buf1Requested = false;
}

void DFDRingBuffer::resize( int aNumChannels, int aNumberOfFrames)
{
    buffer.setSize( aNumChannels, aNumberOfFrames);
    
    inL = (float*) buffer.getReadPointer (0);
    inR = (float*) (buffer.getNumChannels() > 1 ? buffer.getReadPointer (1) : nullptr);
    reset();
}




/*--------------------------------------------------------------------------------------------------------------
 * SampleBinSample
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

SampleBinSample::SampleBinSample(
                                        std::string aName,
                                            int aSampleRate,
                                            int aBitsPerFrame,
                                            int aNumberOfFrames,
                                            int aStartsAtByteInBin
                                        )
:name(aName), sampleRate(aSampleRate), bitsPerFrame(aBitsPerFrame), numberOfFrames(aNumberOfFrames), startsAtByteInBin(aStartsAtByteInBin), numberOfChannels(1),framesBuffer(NULL), memoryMappedReader(NULL)
{
	appliesForDFD = false;
};


SampleBinSample::~SampleBinSample()
{
    resetAudioBuffers();
}


void SampleBinSample::resetAudioBuffers()
{
	//Delete all audio data (buffers...) from the object and keep it alive only with the definition
    if( framesBuffer != NULL) {
        delete framesBuffer;
        framesBuffer = NULL;
    }
    if( memoryMappedReader != NULL) {
        delete memoryMappedReader;
        memoryMappedReader = NULL;
    }
}




/*--------------------------------------------------------------------------------------------------------------
 * SampleBin
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

SampleBin::SampleBin()
: path(""), ready(false)
{
    isDfd = false;
    dfdPreBufferSize = -1;
    dfdBufferSize = 0;
    file = nullptr;
    inStream = nullptr;
	
}

SampleBin::~SampleBin()
{
    resetAudioBuffers();
	deleteRingBuffers();
}

void SampleBin::resetAudioBuffers()
{
    for(std::vector<SampleBinSample>::iterator it = binSamples.begin(); it != binSamples.end(); ++it) {
        it->resetAudioBuffers();
    }
    
}



bool SampleBin::open( std::string aPath)
{
	path = aPath;
    if( path == "")
        return false;
    
    file = new File( path );
    inStream=new FileInputStream( *file );
    
    return inStream->openedOk();
}


void SampleBin::read()
{
	ready = false;
    if( inStream != NULL) {
    
        int fileNameSize = inStream->readInt();
        while(fileNameSize > 0 && !inStream->isExhausted()) {
            std::string aFileName;
            aFileName.resize(fileNameSize);
            char* cFileName = new char[fileNameSize+1];
            inStream->read(cFileName, fileNameSize);
            cFileName[fileNameSize] = 0x00;
            aFileName.assign( cFileName );
			delete[] cFileName;
            
            if( aFileName=="background.png") {
                int pngImgSize = inStream->readInt();
                if( pngImgSize > 0) {
                    char* pngImg = new char[pngImgSize];
                    inStream->read(pngImg, pngImgSize);
                    backgroundImg = ImageCache::getFromMemory( pngImg, pngImgSize );
                    //delete[] pngImg;
                }
            } else if( aFileName.find(".png") != std::string::npos) {
                int pngImgSize = inStream->readInt();
                if( pngImgSize > 0) {
                    char* pngImg = new char[pngImgSize];
                    inStream->read(pngImg, pngImgSize);
                    juce::Image aImg = ImageCache::getFromMemory( pngImg, pngImgSize );
                    int n = aFileName.find(".png");
                    std::string pngName = aFileName.substr(0,n);
                    images[pngName] = aImg;
                    //delete[] pngImg;
                }
                
            } else if( aFileName=="setup.xml") {
                int setupXMLSize = inStream->readInt();
                if( setupXMLSize > 0) {
                    setupXML.reserve(setupXMLSize);
                    char* cSetupXML = new char[setupXMLSize+1];
                    inStream->read(cSetupXML, setupXMLSize);
					cSetupXML[setupXMLSize] = 0x00;
                    setupXML.assign( cSetupXML );
					delete[] cSetupXML;
                }
            } else if( aFileName=="samples") {
				int nrOfSamples;
				nrOfSamples = inStream->readInt();
				
				for( int i=0; i< nrOfSamples; i++) {
					int nSampleNameLength;
					int sampleRate;
					int numberOfFrames;
					int bitsPerFrame;
					int startsAtByteInBin;
					nSampleNameLength = inStream->readInt();
					char* cSampleName;
					cSampleName = new char[nSampleNameLength];
					inStream->read( cSampleName, nSampleNameLength);
					std::string sampleName( cSampleName, nSampleNameLength );
					delete cSampleName;
					sampleRate = inStream->readInt();
					numberOfFrames = inStream->readInt();
					bitsPerFrame = inStream->readInt();
					startsAtByteInBin = inStream->readInt();
					SampleBinSample binSample( sampleName, sampleRate, bitsPerFrame, numberOfFrames, startsAtByteInBin);
					binSamples.push_back(binSample);
				}
				ready = true;
                break;    // break the loop here because after this point come the samples
            } else {
				//Read a block of data that is not supported yet
                int voidSize = inStream->readInt();
                if( voidSize > 0) {
                    char* voidData = new char[voidSize];
                    inStream->read(voidData, voidSize);
                    delete[] voidData;
                }
            }
            fileNameSize = inStream->readInt();
        }
        
    }
}


juce::Image* SampleBin::getImage( std::string aName )
{
    if( images.find( aName ) != images.end() ) {
        return & (images[aName]);
    }
    return nullptr;
}


SampleBinSample* SampleBin::getSample( std::string aName)
{
    for(std::vector<SampleBinSample>::iterator it = binSamples.begin(); it != binSamples.end(); ++it) {
        if( it->name == aName) {
            return &(*it);
        }
    }
    return NULL;
}


SampleBinSample* SampleBin::getSample( int ndx)
{
    if( ndx > -1 && ndx < (int)binSamples.size())
    {
        return &binSamples.at(ndx);
    }
    return NULL;
    
}


void SampleBin::loadAudioData(SampleBinSample* binSample)
{

    if( binSample->framesBuffer == NULL ) { //&& !inStream->isExhausted()) {
        int sizeInFrames = binSample->numberOfFrames;
        if( isDfd && dfdPreBufferSize > 0 && sizeInFrames > dfdPreBufferSize+dfdBufferSize*2 ) {
            binSample->appliesForDFD = true;
            BinAudioFormat binAudioFormat(binSample);
            binSample->memoryMappedReader = binAudioFormat.createMemoryMappedReader(*file);
            bool couldCreateMemoryReader = binSample->memoryMappedReader != NULL;
            if( couldCreateMemoryReader) {
                binSample->memoryMappedReader->mapEntireFile();
                if( binSample->memoryMappedReader->getMappedSection().isEmpty()) {
                    couldCreateMemoryReader = false;
                }
                sizeInFrames = dfdPreBufferSize;
            }
            if( !couldCreateMemoryReader) {
                binSample->appliesForDFD = false;
            }
        }
    
        binSample->framesBuffer = new AudioSampleBuffer( binSample->numberOfChannels, sizeInFrames);
        
        BinAudioFormat binAudioFormat( binSample );
        ScopedPointer<AudioFormatReader> binAudioFormatReader(binAudioFormat.createReaderFor(inStream, false));
        binAudioFormatReader->read(binSample->framesBuffer, 0, sizeInFrames, 0, true, (binSample->numberOfChannels==2));
    }
}


void SampleBin::reloadAudioData()
{
    for(std::vector<SampleBinSample>::iterator it = binSamples.begin(); it != binSamples.end(); ++it) {
        it->resetAudioBuffers();
        loadAudioData( &*it );
    }
}








void SampleBin::setupDfd( bool aIsDfd, int aNrOfRingBuffers, int aDfdPreBufferSize, int aDfdBufferSize, int aDfdLoadFramesAtSamplesLeft)
{
    if( !aIsDfd) {
        isDfd = false;
        dfdPreBufferSize = -1;
        dfdBufferSize = 0;
        dfdNrOfRingBuffers = 0;
        dfdLoadFramesAtSamplesLeft = 0;
    } else {
        isDfd = true;
        dfdPreBufferSize = aDfdPreBufferSize;
        if( dfdPreBufferSize < 1024 ) dfdPreBufferSize = 1024;
        dfdBufferSize = aDfdBufferSize;
        if( dfdBufferSize < 1024 ) dfdBufferSize = 1024;
        dfdNrOfRingBuffers = aNrOfRingBuffers;
        dfdLoadFramesAtSamplesLeft = aDfdLoadFramesAtSamplesLeft;
        if( dfdLoadFramesAtSamplesLeft < 512 ) dfdLoadFramesAtSamplesLeft = 512;
        createRingBuffers();
    }
}

void SampleBin::changeDfd( bool aIsDfd, int aNrOfRingBuffers, int aDfdPreBufferSize, int aDfdBufferSize, int aDfdLoadFramesAtSamplesLeft)
{
    if( isDfd && !aIsDfd) {
        // switch DFD off
        isDfd = false;
        dfdPreBufferSize = -1;
        dfdBufferSize = 0;
        dfdNrOfRingBuffers = 0;
        dfdLoadFramesAtSamplesLeft = 0;
        deleteRingBuffers();
        reloadAudioData();
    } else if( (!isDfd && aIsDfd)  ){
        // switch DFD on
        isDfd = true;
        dfdPreBufferSize = aDfdPreBufferSize;
        dfdBufferSize = aDfdBufferSize;
        dfdNrOfRingBuffers = aNrOfRingBuffers;
        reloadAudioData();
        createRingBuffers();
    } else {
        if( isDfd && aDfdPreBufferSize != dfdPreBufferSize) {
            // change only the preload buffer sizes
            dfdPreBufferSize = aDfdPreBufferSize;
            if( dfdPreBufferSize < 1024 ) dfdPreBufferSize = 1024;
            reloadAudioData();
        }
        if( isDfd && aDfdBufferSize != dfdBufferSize) {
            // change only the buffer size of the ring buffers
            dfdBufferSize = aDfdBufferSize;
            if( dfdBufferSize < 1024 ) dfdBufferSize = 1024;
            deleteRingBuffers();
            createRingBuffers();
        }
        if( isDfd && aNrOfRingBuffers != dfdNrOfRingBuffers) {
            // change only the number of ring Buffers
            dfdNrOfRingBuffers = aNrOfRingBuffers;
            deleteRingBuffers();
            createRingBuffers();
        }
        if (isDfd && aDfdLoadFramesAtSamplesLeft != dfdLoadFramesAtSamplesLeft ) {
        	dfdLoadFramesAtSamplesLeft = aDfdLoadFramesAtSamplesLeft;
            if( dfdLoadFramesAtSamplesLeft < 512 ) dfdLoadFramesAtSamplesLeft = 512;
        }
    }
}




void SampleBin::createRingBuffers()
{
    for( int i=0; i<dfdNrOfRingBuffers; i++) {
        DFDRingBuffer* rbuf = new DFDRingBuffer(1, dfdBufferSize*2);
        dfdRingBuffer.push_back( rbuf );
    }
    
}

void SampleBin::deleteRingBuffers()
{
    for( int i=0; i<(int)dfdRingBuffer.size(); i++) {
        DFDRingBuffer* rbuf = dfdRingBuffer[i];
        delete rbuf;
    }
    dfdRingBuffer.clear();
}

void SampleBin::resizeRingBuffers()
{
    for( int i=0; i<(int)dfdRingBuffer.size(); i++) {
        DFDRingBuffer* rbuf = dfdRingBuffer[i];
        rbuf->buffer.setSize(1, dfdBufferSize);
        rbuf->reset();
    }
}


DFDRingBuffer* SampleBin::requestRingBuffer()
{
    int size = dfdRingBuffer.size();
	for( int i=0;i<size;i++) {
		if( dfdRingBuffer[i]->available ) {
			dfdRingBuffer[i]->available = false;
			return dfdRingBuffer[i];
		}
	}
	return NULL;
}

void SampleBin::returnRingBuffer( DFDRingBuffer* aRingBuffer )
{
	aRingBuffer->available = true;
	aRingBuffer->reset();
}




bool SampleBin::loadBackgroundFromFile( std::string aFileName)
{
    juce::File file( aFileName );
    if( file.existsAsFile()) {
        backgroundImg = ImageCache::getFromFile(file);
        return true;
    }
    return false;
}

bool SampleBin::loadSetupXMLFromFile( std::string aFileName)
{
    File file( aFileName );
    
    FileInputStream aInStream( file );
    
    if( aInStream.failedToOpen() || aInStream.isExhausted())
        return false;
    setupXML = aInStream.readEntireStreamAsString().toStdString();
    return true;
}
















