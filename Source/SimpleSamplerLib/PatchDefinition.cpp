/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchDefinition.cpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#include "PatchDefinition.hpp"
#include "../ProductDef.h"



/*--------------------------------------------------------------------------------------------------------------
 * SoundDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
/*SoundDefinition::SoundDefinition( std::string soundName,
		int noteRangeFrom,
		int noteRangeTo,
		int centerNote,
		bool xFade,
		float gainDb,
        int tuneSt,
		float tuneCt,
		float aPan,
		int aLayer,
		int aSoundSet,
        bool aOneShot)
{
	set(soundName,noteRangeFrom,noteRangeTo,centerNote,xFade,gainDb,tuneSt,tuneCt,aPan,aLayer, aSoundSet,aOneShot);
};*/



SoundDefinition::SoundDefinition()
{
    set( "", false );
	set(-1,-1,-1,0.0f,0,0.0f,0.0f,0,0,false);
}




void SoundDefinition::set( std::string soundName,
         bool xFade)
{
    this->soundName = soundName;
    this->xFade = xFade;
}


void SoundDefinition::set(
		int aNoteRangeFrom,
		int aNoteRangeTo,
		int aCenterNote,
		float aGainDb,
        int aTuneSt,
		float aTuneCt,
		float aPan,
		int aLayer,
		int aSoundSet,
        bool aOneShot)
{
	this->noteRangeFrom = aNoteRangeFrom;
	this->noteRangeTo = aNoteRangeTo;
	this->centerNote = aCenterNote;
	this->gainDb = aGainDb;
    this->tuneSt = aTuneSt;
	this->tuneCt = aTuneCt;
	this->pan = aPan;
	this->layer = aLayer;
	this->soundSet = aSoundSet;
    this->oneShot = aOneShot;
}




void SoundDefinition::addSoundElementDefinition( SoundElementDefinition& aSoundElementDefinition)
{
	soundElementDefinitions.push_back(aSoundElementDefinition);
}


/*void SoundDefinition::addSoundElementDefinition( std::string aSoundName, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo)
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundName );
	soundElementDefinitions.push_back(sed);
}


void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	soundElementDefinitions.push_back(sed);
}
void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3,float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	soundElementDefinitions.push_back(sed);
}
void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, float aGainDb, float aVelocityRangeFrom, float aVelocityRangeTo, float aXFadeVelocityTo )
{
	SoundElementDefinition sed( aGainDb, aVelocityRangeFrom, aVelocityRangeTo, aXFadeVelocityTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR4 );
	soundElementDefinitions.push_back(sed);
}



void SoundDefinition::addSoundElementDefinition( std::string aSoundName, int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundName );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2,int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3,int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	soundElementDefinitions.push_back(sed);
}	

void SoundDefinition::addSoundElementDefinition( std::string aSoundNameRR1, std::string aSoundNameRR2, std::string aSoundNameRR3, std::string aSoundNameRR4, int xFadeSlot, int nXFadeSlots )
{
	float slotWidth = 1.0f / nXFadeSlots;
	float veloRangeFrom = xFadeSlot * slotWidth;
	float veloRangeTo = veloRangeFrom + slotWidth;
	float veloRangeXFadeTo = std::min(veloRangeTo + slotWidth,1.0f);
	
	SoundElementDefinition sed( 0.0f, veloRangeFrom, veloRangeTo, veloRangeXFadeTo);
	sed.addRoundRobinSound( aSoundNameRR1 );
	sed.addRoundRobinSound( aSoundNameRR2 );
	sed.addRoundRobinSound( aSoundNameRR3 );
	sed.addRoundRobinSound( aSoundNameRR4 );
	soundElementDefinitions.push_back(sed);
}	

*/

/*--------------------------------------------------------------------------------------------------------------
 * AHDSREnvelopeDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

AHDSREnvelopeDefinition::AHDSREnvelopeDefinition(float aOffsetT,
		float aAttackT,
		float aHoldT,
		float aDecayT,
		float aSustainDb,
		float aReleaseT)
{
	set( aOffsetT, aAttackT, aHoldT, aDecayT, aSustainDb, aReleaseT );
}


AHDSREnvelopeDefinition::AHDSREnvelopeDefinition()
{
	set(0,0,0,0,0,0);
}

void AHDSREnvelopeDefinition::set (float aOffsetT,
		float aAttackT,
		float aHoldT,
		float aDecayT,
		float aSustainDb,
		float aReleaseT)
{
	offsetT = aOffsetT;
	attackT = aAttackT;
	holdT = aHoldT;
	decayT = aDecayT;
	sustainDb = aSustainDb;
	releaseT = aReleaseT;
}




/*--------------------------------------------------------------------------------------------------------------
 * KeyswitchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

KeyswitchDefinition::KeyswitchDefinition( std::string aName, int aKey, bool aIsPermanent, KEYSWITCHTYPE aType, int aParam)
{
    name = aName;
    key = aKey;
    type = aType;
    param = aParam;
    isPermanent = aIsPermanent;
}


/*--------------------------------------------------------------------------------------------------------------
 * KeyGroupDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

 
KeyGroupDefinition::KeyGroupDefinition()
{
    name = "";
    isMono = false;
    fromKey = 0;
    toKey = 128;
    gainDb = 0.0;
    pan = 0.0;
    tuneSt = 0.0;
    tuneCt = 0.0;
	bevel = 0.0;
}

KeyGroupDefinition::KeyGroupDefinition( std::string aName, bool aIsMono, int aFromKey, int aToKey, float aGainDb, float aPan, int aTuneSt, float aTuneCt, float aBevel)
{
    name = aName;
    isMono = aIsMono;
    fromKey = aFromKey;
    toKey = aToKey;
    gainDb = aGainDb;
    pan = aPan;
    tuneSt = aTuneSt;
    tuneCt = aTuneCt;
	bevel = aBevel;
}





/*--------------------------------------------------------------------------------------------------------------
 * PatchDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

/*PatchDefinition::PatchDefinition(std::string aName)
{
	init();
	name = aName;
	numberOfVoices = 0;
}*/

PatchDefinition::PatchDefinition()
{
	init();
}

void PatchDefinition::init()
{
	name = "";
	numberOfVoices = 0;
	gainDb = 0.0f;
	pan = 0.0f;
	tune = 0.0f;
    tuneSt = 0;
	veloSens = 1.0;
	keySwitchRangeFromMidiNote = -1;
	keySwitchRangeToMidiNote = -1;
	playRangeFromMidiNote = 0;
	playRangeToMidiNote = 127;
	pressureMode = PRESSUREMODE::VELOCITY;
	pressureController = 1;

    layerGainDb.resize(MAX_NR_OF_LAYERS);
    for( int i=0; i<MAX_NR_OF_LAYERS; i++) {
        layerGainDb[i] = 0.0f;
    }
    
	pitchWheelMode = PITCHWHEELMODE::NOPITCH;
	pitchWheelRangeSt = 12;
	pitchWheelRetriggerReleaseS = 0.0f;
	pitchWheelRetriggerAttackS = 0.0f;
	pitchWheelRetriggerOffsetS = 0.0f;

	legatoMode = LEGATOMODE::NOLEGATO;
	legatoReleaseS = 0.0f;
	legatoOffsetS = 0.0f;
	legatoAttackS = 0.0f;

	monoMode = false;
	dfd = false;
	dfdPreloadFrames = 0;
    dfdLoadFrames = 0;
    dfdNrOfRingBuffers = 0;
}


PatchDefinition::~PatchDefinition()
{
}



void PatchDefinition::setName( std::string aName)
{
	name = aName;
}


void PatchDefinition::addSoundDefinitionName( std::string& aName )
{
    soundDefinitionNames.push_back(aName);
}

int PatchDefinition::getNrOfSoundDefinitionNames()
{
    return soundDefinitionNames.size();
}

std::string& PatchDefinition::getSoundDefinitionNameAt( int ndx )
{
    return soundDefinitionNames.at( ndx );
}

void PatchDefinition::addSoundDefinition( SoundDefinition& aSoundDefinition)
{
    soundDefinitions.push_back(aSoundDefinition);
}

int PatchDefinition::getNrOfSoundDefinitions()
{
    return soundDefinitions.size();
}

SoundDefinition* PatchDefinition::getSoundDefinitionAt( int ndx )
{
    return &soundDefinitions.at( ndx );
}

/*
void PatchDefinition::addSoundDefinition( SoundDefinition& aSoundDefinition)
{
	soundDefinitions.push_back(aSoundDefinition);
}


SoundDefinition* PatchDefinition::createAndAddSoundDefinition()
{
	SoundDefinition sd;
	soundDefinitions.push_back(sd);

	return &soundDefinitions[ soundDefinitions.size()-1];

}


int PatchDefinition::getNrOfSoundDefinitions()
{
    return soundDefinitions.size();
}

SoundDefinition& PatchDefinition::getSoundDefinitionAt( int ndx )
{
    return soundDefinitions.at( ndx );
}
*/

void PatchDefinition::addKeyswitchDefinition( KeyswitchDefinition& aKeyswitchDefinition)
{
	keyswitchDefinitions.push_back(aKeyswitchDefinition);
	if( aKeyswitchDefinition.type == KeyswitchDefinition::KEYSWITCHTYPE_SOUNDSET) {
		soundSetNames.add( aKeyswitchDefinition.name );
	}
}

void PatchDefinition::addKeyswitchDefinition( std::string aName, int aMidiNote, bool aIsPermanent, KeyswitchDefinition::KEYSWITCHTYPE aType, int aParam)
{
	KeyswitchDefinition ks( aName, aMidiNote, aIsPermanent, aType, aParam);
	addKeyswitchDefinition(ks);
}


void PatchDefinition::setEnvelopeDefinition(AHDSREnvelopeDefinition& aEnvelopeDefinition)
{
	envelopeDefinition = aEnvelopeDefinition;
}

void PatchDefinition::setEnvelopeDefinition(float aOffsetT, float aAttackT, float aHoldT, float aDecayT, float aSustainDb, float aReleaseT)
{
	envelopeDefinition.set(aOffsetT, aAttackT, aHoldT, aDecayT, aSustainDb, aReleaseT);
}





AHDSREnvelopeDefinition& PatchDefinition::getEnvelopeDefinition()
{
	return envelopeDefinition;
}

void PatchDefinition::setPitchWheelDefinition( PITCHWHEELMODE aPitchWheelMode, int aPitchWheelRangeSt, float aRetriggerReleaseS, float aRetriggerOffsetS, float aRetriggerAttackS )
{
	pitchWheelMode = aPitchWheelMode;
	pitchWheelRangeSt = aPitchWheelRangeSt;
	pitchWheelRetriggerReleaseS = aRetriggerReleaseS;
	pitchWheelRetriggerOffsetS = aRetriggerOffsetS;
	pitchWheelRetriggerAttackS = aRetriggerAttackS;
}		


void PatchDefinition::setLegatoDefinition( LEGATOMODE aLegatoMode, float aLegatoReleaseS, float aLegatoOffsetS, float aLegatoAttackS )
{
	legatoMode = aLegatoMode;
	legatoReleaseS = aLegatoReleaseS;
	legatoOffsetS = aLegatoOffsetS;
	legatoAttackS = aLegatoAttackS;
}



void PatchDefinition::setDfd( bool aDfd, int aNrOfRingBuffers, int aDfdPreloadFrames, int aDfdLoadFrames, int aDfdLoadAtFramesLeft)
{
	dfd = aDfd;
	dfdPreloadFrames = aDfdPreloadFrames;
    dfdLoadFrames = aDfdLoadFrames;
    dfdNrOfRingBuffers = aNrOfRingBuffers;
    dfdLoadAtFramesLeft = aDfdLoadAtFramesLeft;
}

/*
void PatchDefinition::addMonoGroupRange( int fromNoteNr, int toNoteNr )
{
	Range<int> r(fromNoteNr, toNoteNr);
	monoGroupRanges.push_back( r );
}
*/

void PatchDefinition::addKeyGroup( std::string aName, bool aIsMono, int aFromKey, int aToKey, float aRelGainDb, float aRelPan, int aRelTuneSt, float aRelTuneCt, float aBevel)
{
    KeyGroupDefinition kgd( aName, aIsMono, aFromKey, aToKey, aRelGainDb, aRelPan, aRelTuneSt, aRelTuneCt, aBevel);
    keyGroups.push_back(kgd);
    
}


float PatchDefinition::getLayerGainDb( int aLayerNr )
{
    if( layerGainDb.size() > aLayerNr && aLayerNr > -1) {
        return layerGainDb[aLayerNr];
    }
    return 0.0f;
}

void PatchDefinition::setLayerGainDb( int aLayerNr, float aGainDb )
{
    if( layerGainDb.size() > aLayerNr && aLayerNr > -1) {
        layerGainDb[aLayerNr] = aGainDb;
    }
}






void PatchBank::clear()
{
    patchDefinitionsVector.clear();
    soundDefinitionsMap.clear();
}


void PatchBank::addPatchDefinition( PatchDefinition& aPatchDefinition)
{
    patchDefinitionsVector.push_back(aPatchDefinition);
}



PatchDefinition* PatchBank::getPatchDefinitionAt( std::string& aName)
{
    int n = (int)patchDefinitionsVector.size();
    for( int i=0; i<n; i++) {
        if( patchDefinitionsVector[i].getName() == aName)  {
            return &patchDefinitionsVector[i];
        }
    }
    return nullptr;
}

PatchDefinition* PatchBank::getPatchDefinitionAt( int aNdx)
{
    if( aNdx > -1 && patchDefinitionsVector.size() > aNdx) {
        return &patchDefinitionsVector[aNdx];
    }
    return nullptr;
}

int PatchBank::getPatchDefinitionNdx( std::string& aName)
{
    int n = (int)patchDefinitionsVector.size();
    for( int i=0; i<n; i++) {
        if( patchDefinitionsVector[i].getName() == aName)  {
            return i;
        }
    }
    return -1;
}


int PatchBank::getNrOfPatchDefinitions()
{
    return (int)patchDefinitionsVector.size();
}


void PatchBank::addSoundDefinition( SoundDefinition& aSoundDefinition)
{
    if( soundDefinitionsMap.find( aSoundDefinition.soundName  ) == soundDefinitionsMap.end() ) {
        soundDefinitionsMap[aSoundDefinition.soundName] = aSoundDefinition;
    }
    
}


SoundDefinition* PatchBank::getSoundDefinitionAt( std::string& aName)
{
    if( soundDefinitionsMap.find( aName ) != soundDefinitionsMap.end() ) {
        return &soundDefinitionsMap[aName];
    }
    return nullptr;
}


/*void PatchBank::orchestratePatch( int aNdx )
{
    ScopedPointer<PatchDefinition> pd = patchDefinitionsVector[aNdx];
    int n = pd->getNrOfSoundDefinitionNames();
    for( int i = 0; i<n; i++) {
        std::string& aSoundName = pd->getSoundDefinitionNameAt(i);
        SoundDefinition* aSoundDef = getSoundDefinition(aSoundName);
        
        ScopedPointer<SoundDefinition> sdClone = new SoundDefinition();
        *sdClone = *aSoundDef;
        
        if( aSoundDef != nullptr ) {
            pd->addSoundDefinition(aSoundDef);
        }
        
    }
    
}

void PatchBank::orchestrateAllPatches()
{
    int n = (int)patchDefinitionsVector.size();
    for( int i=0; i<n; i++) {
        orchestratePatch( i );
    }
}*/

