/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  XFadeSamplerSound.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 03/12/16.
//
//

#include "XFadeSamplerSound.hpp"




/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerSound
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

XFadeSamplerSound::XFadeSamplerSound(
                                     SoundDefinition* aSoundDefinition,
                                     SampleBin* aSampleBin,
                                     std::string& errors)
{
    ready = true;
	soundDefinition = aSoundDefinition;

	int n = soundDefinition->soundElementDefinitions.size();
	for( int i=0; i<n ; i++ ){
		SoundElementDefinition& sed =soundDefinition->soundElementDefinitions.at(i);

		XFadeSamplerSoundElement sse;
		sse.roundRobinMode = sed.roundRobinMode;

		for( int j = 0; j<sed.getNrOfRoundRobinSounds(); j++) {
            std::string* sampleName =sed.getRoundRobinSound(j);
            if( sampleName != NULL) {
                SampleBinSample* sbs = aSampleBin->getSample(*sampleName);
                if( sbs != NULL) {
                    aSampleBin->loadAudioData(sbs);
                    sse.addRoundrobinBinSample( sbs );
                } else {
                    ready = false;
                    errors.append( *sampleName + "\n");
                }
            }
		}
        soundElements.push_back(sse);
	}
}

XFadeSamplerSound::~XFadeSamplerSound()
{
}



bool XFadeSamplerSound::appliesTo( int aSoundSet, int midiNoteNumber )
{
	bool appliesToMidiNote = appliesToNote( midiNoteNumber );
	bool appliesToKeySwitch = ( aSoundSet == soundDefinition->soundSet);

	return appliesToMidiNote  && appliesToKeySwitch;
}


bool XFadeSamplerSound::appliesToNote (int midiNoteNumber)
{
	return( midiNoteNumber >= soundDefinition->noteRangeFrom && midiNoteNumber <= soundDefinition->noteRangeTo);
}

bool XFadeSamplerSound::appliesToChannel (int /*midiChannel*/)
{
	return true;
}


